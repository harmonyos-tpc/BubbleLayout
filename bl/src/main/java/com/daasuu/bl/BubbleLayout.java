package com.daasuu.bl;

import com.daasuu.bl.utils.AttrUtils;

import ohos.agp.components.*;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

public class BubbleLayout extends StackLayout implements Component.DrawTask {
    public static float DEFAULT_STROKE_WIDTH = -1;
    private ArrowDirection mArrowDirection;
    private Bubble mBubble;

    private float mArrowWidth;
    private float mArrowHeight;
    private float mArrowPosition;
    private float mStrokeWidth;
    private float mCornersRadius;
    private int mBubbleColor;
    private int mStrokeColor;
    private int componentWidth = 0;
    private int componentHeight = 0;

    public BubbleLayout(Context context) {
        this(context, null, null);
    }

    public BubbleLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public BubbleLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet, styleName);
    }

    private void init(Context context, AttrSet attrSet, String styleName) {
        mArrowWidth = AttrUtils.getDimensionFromAttr(attrSet, "bl_arrowWidth", AttrHelper.vp2px(8, context));
        mArrowHeight = AttrUtils.getDimensionFromAttr(attrSet, "bl_arrowHeight", AttrHelper.vp2px(8, context));
        mCornersRadius = AttrUtils.getDimensionFromAttr(attrSet, "bl_cornersRadius", AttrHelper.vp2px(12, context));
        mArrowPosition = AttrUtils.getDimensionFromAttr(attrSet, "bl_arrowPosition", AttrHelper.vp2px(12, context));

        mBubbleColor = AttrUtils.getColorFromAttr(attrSet, "bl_bubbleColor", Color.WHITE.getValue());
        mStrokeColor = AttrUtils.getColorFromAttr(attrSet, "bl_strokeColor", Color.GRAY.getValue());
        mStrokeWidth = AttrUtils.getDimensionFromAttr(attrSet, "bl_strokeWidth", (int) DEFAULT_STROKE_WIDTH);

        String locationStr = AttrUtils.getStringFromAttr(attrSet, "bl_arrowDirection", "left");
        mArrowDirection = ArrowDirection.fromInt(getArrowDirection(locationStr));

        addDrawTask(this, 1);
        initPadding();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.save();
        componentWidth = component.getWidth();
        componentHeight = component.getHeight();
        initDrawable(0, componentWidth, 0, componentHeight);
        if (mBubble != null) {
            mBubble.onDraw(component, canvas);
        }
        canvas.restore();
    }

    private void initDrawable(int left, int right, int top, int bottom) {
        if (right < left || bottom < top) return;

        RectFloat rectF = new RectFloat(left, top, right, bottom);
        float arrowPosition = mArrowPosition;
        switch (mArrowDirection) {
            case LEFT_CENTER:
            case RIGHT_CENTER:
                arrowPosition = (bottom - top) / 2f - mArrowHeight / 2;
                break;
            case TOP_CENTER:
            case BOTTOM_CENTER:
                arrowPosition = (right - left) / 2f - mArrowWidth / 2;
                break;
            case TOP_RIGHT:
            case BOTTOM_RIGHT:
                arrowPosition = right - mArrowPosition - mArrowWidth / 2;
            default:
                break;
        }
        mBubble = new Bubble(
                        rectF,
                        mArrowWidth,
                        mCornersRadius,
                        mArrowHeight,
                        arrowPosition,
                        mStrokeWidth,
                        mStrokeColor,
                        mBubbleColor,
                        mArrowDirection);
    }

    private void initPadding() {
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        switch (mArrowDirection) {
            case LEFT:
            case LEFT_CENTER:
                paddingLeft += mArrowWidth;
                break;
            case RIGHT:
            case RIGHT_CENTER:
                paddingRight += mArrowWidth;
                break;
            case TOP:
            case TOP_CENTER:
            case TOP_RIGHT:
                paddingTop += mArrowHeight;
                break;
            case BOTTOM:
            case BOTTOM_CENTER:
            case BOTTOM_RIGHT:
                paddingBottom += mArrowHeight;
                break;
        }
        if (mStrokeWidth > 0) {
            paddingLeft += mStrokeWidth;
            paddingRight += mStrokeWidth;
            paddingTop += mStrokeWidth;
            paddingBottom += mStrokeWidth;
        }
        setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
    }

    private void resetPadding() {
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        switch (mArrowDirection) {
            case LEFT:
            case LEFT_CENTER:
                paddingLeft -= mArrowWidth;
                break;
            case RIGHT:
            case RIGHT_CENTER:
                paddingRight -= mArrowWidth;
                break;
            case TOP:
            case TOP_CENTER:
            case TOP_RIGHT:
                paddingTop -= mArrowHeight;
                break;
            case BOTTOM:
            case BOTTOM_CENTER:
            case BOTTOM_RIGHT:
                paddingBottom -= mArrowHeight;
                break;
        }
        if (mStrokeWidth > 0) {
            paddingLeft -= mStrokeWidth;
            paddingRight -= mStrokeWidth;
            paddingTop -= mStrokeWidth;
            paddingBottom -= mStrokeWidth;
        }
        setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
    }

    public BubbleLayout setArrowDirection(ArrowDirection arrowDirection) {
        resetPadding();
        mArrowDirection = arrowDirection;
        initPadding();
        return this;
    }

    public BubbleLayout setArrowWidth(float arrowWidth) {
        resetPadding();
        mArrowWidth = arrowWidth;
        initPadding();
        return this;
    }

    public BubbleLayout setCornersRadius(float cornersRadius) {
        mCornersRadius = cornersRadius;
        postLayout();
        return this;
    }

    public BubbleLayout setArrowHeight(float arrowHeight) {
        resetPadding();
        mArrowHeight = arrowHeight;
        initPadding();
        return this;
    }

    public BubbleLayout setArrowPosition(float arrowPosition) {
        resetPadding();
        mArrowPosition = arrowPosition;
        initPadding();
        return this;
    }

    public BubbleLayout setBubbleColor(int bubbleColor) {
        mBubbleColor = bubbleColor;
        postLayout();
        return this;
    }

    public BubbleLayout setStrokeWidth(float strokeWidth) {
        resetPadding();
        mStrokeWidth = strokeWidth;
        initPadding();
        return this;
    }

    public BubbleLayout setStrokeColor(int strokeColor) {
        mStrokeColor = strokeColor;
        postLayout();
        return this;
    }

    public ArrowDirection getArrowDirection() {
        return mArrowDirection;
    }

    public float getArrowWidth() {
        return mArrowWidth;
    }

    public float getCornersRadius() {
        return mCornersRadius;
    }

    public float getArrowHeight() {
        return mArrowHeight;
    }

    public float getArrowPosition() {
        return mArrowPosition;
    }

    public int getBubbleColor() {
        return mBubbleColor;
    }

    public float getStrokeWidth() {
        return mStrokeWidth;
    }

    public int getStrokeColor() {
        return mStrokeColor;
    }

    private int getArrowDirection(String locationStr) {
        if (locationStr.endsWith("left")) {
            return 0;
        } else if (locationStr.endsWith("right")) {
            return 1;
        } else if (locationStr.endsWith("top")) {
            return 2;
        } else if (locationStr.endsWith("bottom")) {
            return 3;
        } else if (locationStr.endsWith("left_center")) {
            return 4;
        } else if (locationStr.endsWith("right_center")) {
            return 5;
        } else if (locationStr.endsWith("top_center")) {
            return 6;
        } else if (locationStr.endsWith("bottom_center")) {
            return 7;
        } else if (locationStr.endsWith("top_right")) {
            return 9;
        } else {
            return 1;
        }
    }
}
