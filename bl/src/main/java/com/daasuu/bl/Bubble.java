package com.daasuu.bl;

import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;

public class Bubble {
    private RectFloat mRect;
    private Path mPath = new Path();
    private Path mStrokePath;
    private Paint mPaint = new Paint();
    private Paint mStrokePaint;

    private float mArrowWidth;
    private float mArrowHeight;
    private float mCornersRadius;
    private float mArrowPosition;
    private float mStrokeWidth;

    public Bubble(
            RectFloat rect,
            float arrowWidth,
            float cornersRadius,
            float arrowHeight,
            float arrowPosition,
            float strokeWidth,
            int strokeColor,
            int bubbleColor,
            ArrowDirection arrowDirection) {
        this.mRect = rect;
        this.mArrowWidth = arrowWidth;
        this.mCornersRadius = cornersRadius;
        this.mArrowHeight = arrowHeight;
        this.mArrowPosition = arrowPosition;
        this.mStrokeWidth = strokeWidth;

        mPaint.setColor(new Color(bubbleColor));
        mPaint.setAntiAlias(true);

        if (strokeWidth > 0) {
            mStrokePaint = new Paint();
            mStrokePaint.setAntiAlias(true);
            mStrokePaint.setStrokeWidth(2);
            mStrokePaint.setColor(new Color(strokeColor));

            mStrokePath = new Path();
            initPath(arrowDirection, mPath, strokeWidth);
            initPath(arrowDirection, mStrokePath, 0);
        } else {
            initPath(arrowDirection, mPath, 0);
        }
    }

    public void onDraw(Component component, Canvas canvas) {
        if (mStrokeWidth > 0) {
            canvas.drawPath(mStrokePath, mStrokePaint);
        }
        canvas.drawPath(mPath, mPaint);
    }

    private void initPath(ArrowDirection arrowDirection, Path path, float strokeWidth) {
        switch (arrowDirection) {
            case LEFT:
            case LEFT_CENTER:
                if (mCornersRadius <= 0) {
                    initLeftSquarePath(mRect, path, strokeWidth);
                    break;
                }

                if (strokeWidth > 0 && strokeWidth > mCornersRadius) {
                    initLeftSquarePath(mRect, path, strokeWidth);
                    break;
                }
                initLeftRoundedPath(mRect, path, strokeWidth);
                break;
            case TOP:
            case TOP_CENTER:
            case TOP_RIGHT:
                if (mCornersRadius <= 0) {
                    initTopSquarePath(mRect, path, strokeWidth);
                    break;
                }

                if (strokeWidth > 0 && strokeWidth > mCornersRadius) {
                    initTopSquarePath(mRect, path, strokeWidth);
                    break;
                }

                initTopRoundedPath(mRect, path, strokeWidth);

                break;
            case RIGHT:
            case RIGHT_CENTER:
                if (mCornersRadius <= 0) {
                    initRightSquarePath(mRect, path, strokeWidth);
                    break;
                }

                if (strokeWidth > 0 && strokeWidth > mCornersRadius) {
                    initRightSquarePath(mRect, path, strokeWidth);
                    break;
                }

                initRightRoundedPath(mRect, path, strokeWidth);

                break;
            case BOTTOM:
            case BOTTOM_CENTER:
            case BOTTOM_RIGHT:
                if (mCornersRadius <= 0) {
                    initBottomSquarePath(mRect, path, strokeWidth);
                    break;
                }

                if (strokeWidth > 0 && strokeWidth > mCornersRadius) {
                    initBottomSquarePath(mRect, path, strokeWidth);
                    break;
                }

                initBottomRoundedPath(mRect, path, strokeWidth);
                break;
        }
    }

    private void initLeftRoundedPath(RectFloat rect, Path path, float strokeWidth) {
        path.moveTo(mArrowWidth + rect.left + mCornersRadius + strokeWidth, rect.top + strokeWidth);
        path.lineTo(rect.getWidth() - mCornersRadius - strokeWidth, rect.top + strokeWidth);
        path.arcTo(
                new RectFloat(
                        rect.right - mCornersRadius,
                        rect.top + strokeWidth,
                        rect.right - strokeWidth,
                        mCornersRadius + rect.top),
                270,
                90);

        path.lineTo(rect.right - strokeWidth, rect.bottom - mCornersRadius - strokeWidth);
        path.arcTo(
                new RectFloat(
                        rect.right - mCornersRadius,
                        rect.bottom - mCornersRadius,
                        rect.right - strokeWidth,
                        rect.bottom - strokeWidth),
                0,
                90);

        path.lineTo(rect.left + mArrowWidth + mCornersRadius + strokeWidth, rect.bottom - strokeWidth);

        path.arcTo(
                new RectFloat(
                        rect.left + mArrowWidth + strokeWidth,
                        rect.bottom - mCornersRadius,
                        mCornersRadius + rect.left + mArrowWidth,
                        rect.bottom - strokeWidth),
                90,
                90);

        path.lineTo(rect.left + mArrowWidth + strokeWidth, mArrowHeight + mArrowPosition - (strokeWidth / 2));

        path.lineTo(rect.left + strokeWidth + strokeWidth, mArrowPosition + mArrowHeight / 2);

        path.lineTo(rect.left + mArrowWidth + strokeWidth, mArrowPosition + (strokeWidth / 2));

        path.lineTo(rect.left + mArrowWidth + strokeWidth, rect.top + mCornersRadius + strokeWidth);

        path.arcTo(
                new RectFloat(
                        rect.left + mArrowWidth + strokeWidth,
                        rect.top + strokeWidth,
                        mCornersRadius + rect.left + mArrowWidth,
                        mCornersRadius + rect.top),
                180,
                90);

        path.close();
    }

    private void initLeftSquarePath(RectFloat rect, Path path, float strokeWidth) {
        path.moveTo(mArrowWidth + rect.left + strokeWidth, rect.top + strokeWidth);
        path.lineTo(rect.getWidth() - strokeWidth, rect.top + strokeWidth);

        path.lineTo(rect.right - strokeWidth, rect.bottom - strokeWidth);

        path.lineTo(rect.left + mArrowWidth + strokeWidth, rect.bottom - strokeWidth);

        path.lineTo(rect.left + mArrowWidth + strokeWidth, mArrowHeight + mArrowPosition - (strokeWidth / 2));
        path.lineTo(rect.left + strokeWidth + strokeWidth, mArrowPosition + mArrowHeight / 2);
        path.lineTo(rect.left + mArrowWidth + strokeWidth, mArrowPosition + (strokeWidth / 2));

        path.lineTo(rect.left + mArrowWidth + strokeWidth, rect.top + strokeWidth);

        path.close();
    }

    private void initTopRoundedPath(RectFloat rect, Path path, float strokeWidth) {
        path.moveTo(
                rect.left + Math.min(mArrowPosition, mCornersRadius) + strokeWidth,
                rect.top + mArrowHeight + strokeWidth);
        path.lineTo(rect.left + mArrowPosition + (strokeWidth / 2), rect.top + mArrowHeight + strokeWidth);
        path.lineTo(rect.left + mArrowWidth / 2 + mArrowPosition, rect.top + strokeWidth + strokeWidth);
        path.lineTo(
                rect.left + mArrowWidth + mArrowPosition - (strokeWidth / 2), rect.top + mArrowHeight + strokeWidth);
        path.lineTo(rect.right - mCornersRadius - strokeWidth, rect.top + mArrowHeight + strokeWidth);

        path.arcTo(
                new RectFloat(
                        rect.right - mCornersRadius,
                        rect.top + mArrowHeight + strokeWidth,
                        rect.right - strokeWidth,
                        mCornersRadius + rect.top + mArrowHeight),
                270,
                90);
        path.lineTo(rect.right - strokeWidth, rect.bottom - mCornersRadius - strokeWidth);

        path.arcTo(
                new RectFloat(
                        rect.right - mCornersRadius,
                        rect.bottom - mCornersRadius,
                        rect.right - strokeWidth,
                        rect.bottom - strokeWidth),
                0,
                90);
        path.lineTo(rect.left + mCornersRadius + strokeWidth, rect.bottom - strokeWidth);

        path.arcTo(
                new RectFloat(
                        rect.left + strokeWidth,
                        rect.bottom - mCornersRadius,
                        mCornersRadius + rect.left,
                        rect.bottom - strokeWidth),
                90,
                90);

        path.lineTo(rect.left + strokeWidth, rect.top + mArrowHeight + mCornersRadius + strokeWidth);

        path.arcTo(
                new RectFloat(
                        rect.left + strokeWidth,
                        rect.top + mArrowHeight + strokeWidth,
                        mCornersRadius + rect.left,
                        mCornersRadius + rect.top + mArrowHeight),
                180,
                90);

        path.close();
    }

    private void initTopSquarePath(RectFloat rect, Path path, float strokeWidth) {
        path.moveTo(rect.left + mArrowPosition + strokeWidth, rect.top + mArrowHeight + strokeWidth);

        path.lineTo(rect.left + mArrowPosition + (strokeWidth / 2), rect.top + mArrowHeight + strokeWidth);
        path.lineTo(rect.left + mArrowWidth / 2 + mArrowPosition, rect.top + strokeWidth + strokeWidth);
        path.lineTo(
                rect.left + mArrowWidth + mArrowPosition - (strokeWidth / 2), rect.top + mArrowHeight + strokeWidth);
        path.lineTo(rect.right - strokeWidth, rect.top + mArrowHeight + strokeWidth);

        path.lineTo(rect.right - strokeWidth, rect.bottom - strokeWidth);

        path.lineTo(rect.left + strokeWidth, rect.bottom - strokeWidth);

        path.lineTo(rect.left + strokeWidth, rect.top + mArrowHeight + strokeWidth);

        path.lineTo(rect.left + mArrowPosition + strokeWidth, rect.top + mArrowHeight + strokeWidth);

        path.close();
    }

    private void initRightRoundedPath(RectFloat rect, Path path, float strokeWidth) {
        path.moveTo(rect.left + mCornersRadius + strokeWidth, rect.top + strokeWidth);
        path.lineTo(rect.getWidth() - mCornersRadius - mArrowWidth - strokeWidth, rect.top + strokeWidth);
        path.arcTo(
                new RectFloat(
                        rect.right - mCornersRadius - mArrowWidth,
                        rect.top + strokeWidth,
                        rect.right - mArrowWidth - strokeWidth,
                        mCornersRadius + rect.top),
                270,
                90);

        path.lineTo(rect.right - mArrowWidth - strokeWidth, mArrowPosition + (strokeWidth / 2));
        path.lineTo(rect.right - strokeWidth - strokeWidth, mArrowPosition + mArrowHeight / 2);
        path.lineTo(rect.right - mArrowWidth - strokeWidth, mArrowPosition + mArrowHeight - (strokeWidth / 2));
        path.lineTo(rect.right - mArrowWidth - strokeWidth, rect.bottom - mCornersRadius - strokeWidth);

        path.arcTo(
                new RectFloat(
                        rect.right - mCornersRadius - mArrowWidth,
                        rect.bottom - mCornersRadius,
                        rect.right - mArrowWidth - strokeWidth,
                        rect.bottom - strokeWidth),
                0,
                90);
        path.lineTo(rect.left + mArrowWidth + strokeWidth, rect.bottom - strokeWidth);

        path.arcTo(
                new RectFloat(
                        rect.left + strokeWidth,
                        rect.bottom - mCornersRadius,
                        mCornersRadius + rect.left,
                        rect.bottom - strokeWidth),
                90,
                90);

        path.arcTo(
                new RectFloat(
                        rect.left + strokeWidth,
                        rect.top + strokeWidth,
                        mCornersRadius + rect.left,
                        mCornersRadius + rect.top),
                180,
                90);
        path.close();
    }

    private void initRightSquarePath(RectFloat rect, Path path, float strokeWidth) {
        path.moveTo(rect.left + strokeWidth, rect.top + strokeWidth);
        path.lineTo(rect.getWidth() - mArrowWidth - strokeWidth, rect.top + strokeWidth);

        path.lineTo(rect.right - mArrowWidth - strokeWidth, mArrowPosition + (strokeWidth / 2));
        path.lineTo(rect.right - strokeWidth - strokeWidth, mArrowPosition + mArrowHeight / 2);
        path.lineTo(rect.right - mArrowWidth - strokeWidth, mArrowPosition + mArrowHeight - (strokeWidth / 2));

        path.lineTo(rect.right - mArrowWidth - strokeWidth, rect.bottom - strokeWidth);

        path.lineTo(rect.left + strokeWidth, rect.bottom - strokeWidth);
        path.lineTo(rect.left + strokeWidth, rect.top + strokeWidth);

        path.close();
    }

    private void initBottomRoundedPath(RectFloat rect, Path path, float strokeWidth) {
        path.moveTo(rect.left + mCornersRadius + strokeWidth, rect.top + strokeWidth);
        path.lineTo(rect.getWidth() - mCornersRadius - strokeWidth, rect.top + strokeWidth);
        path.arcTo(
                new RectFloat(
                        rect.right - mCornersRadius,
                        rect.top + strokeWidth,
                        rect.right - strokeWidth,
                        mCornersRadius + rect.top),
                270,
                90);

        path.lineTo(rect.right - strokeWidth, rect.bottom - mArrowHeight - mCornersRadius - strokeWidth);
        path.arcTo(
                new RectFloat(
                        rect.right - mCornersRadius,
                        rect.bottom - mCornersRadius - mArrowHeight,
                        rect.right - strokeWidth,
                        rect.bottom - mArrowHeight - strokeWidth),
                0,
                90);

        path.lineTo(
                rect.left + mArrowWidth + mArrowPosition - (strokeWidth / 2), rect.bottom - mArrowHeight - strokeWidth);
        path.lineTo(rect.left + mArrowPosition + mArrowWidth / 2, rect.bottom - strokeWidth - strokeWidth);
        path.lineTo(rect.left + mArrowPosition + (strokeWidth / 2), rect.bottom - mArrowHeight - strokeWidth);
        path.lineTo(
                rect.left + Math.min(mCornersRadius, mArrowPosition) + strokeWidth,
                rect.bottom - mArrowHeight - strokeWidth);

        path.arcTo(
                new RectFloat(
                        rect.left + strokeWidth,
                        rect.bottom - mCornersRadius - mArrowHeight,
                        mCornersRadius + rect.left,
                        rect.bottom - mArrowHeight - strokeWidth),
                90,
                90);
        path.lineTo(rect.left + strokeWidth, rect.top + mCornersRadius + strokeWidth);
        path.arcTo(
                new RectFloat(
                        rect.left + strokeWidth,
                        rect.top + strokeWidth,
                        mCornersRadius + rect.left,
                        mCornersRadius + rect.top),
                180,
                90);
        path.close();
    }

    private void initBottomSquarePath(RectFloat rect, Path path, float strokeWidth) {
        path.moveTo(rect.left + strokeWidth, rect.top + strokeWidth);
        path.lineTo(rect.right - strokeWidth, rect.top + strokeWidth);
        path.lineTo(rect.right - strokeWidth, rect.bottom - mArrowHeight - strokeWidth);

        path.lineTo(
                rect.left + mArrowWidth + mArrowPosition - (strokeWidth / 2), rect.bottom - mArrowHeight - strokeWidth);
        path.lineTo(rect.left + mArrowPosition + mArrowWidth / 2, rect.bottom - strokeWidth - strokeWidth);
        path.lineTo(rect.left + mArrowPosition + (strokeWidth / 2), rect.bottom - mArrowHeight - strokeWidth);
        path.lineTo(rect.left + mArrowPosition + strokeWidth, rect.bottom - mArrowHeight - strokeWidth);

        path.lineTo(rect.left + strokeWidth, rect.bottom - mArrowHeight - strokeWidth);
        path.lineTo(rect.left + strokeWidth, rect.top + strokeWidth);
        path.close();
    }
}
