# BubbleLayout
Bubble Component for openharmony with custom stroke width and color, arrow size, position and direction.<br>
BubbleLayout Extends the StackLayout.

# Gradle Dependency
```groovy
dependencies {
    implementation "io.openharmony.tpc.thirdlib:BubbleLayout:1.0.2"

}
```

# Basic Usage
Include the BubbleLayout widget in your layout.

```xml
<com.daasuu.bl.BubbleLayout
        ohos:width="match_content"
        ohos:height="match_content"
        ohos:padding="8vp"
        app:bl_arrowDirection="left"
        app:bl_arrowHeight="8vp"
        app:bl_arrowPosition="16vp"
        app:bl_arrowWidth="8vp"
        app:bl_strokeWidth="1vp"
        app:bl_strokeColor="#ffcc0000">

        <Text
            ohos:width="match_content"
            ohos:height="match_content"
            ohos:layout_alignment="vertical_center"
            ohos:text="BubbleLayout"
            ohos:text_size="16fp"
            ohos:text_color="#ffcc0000"
            />
    </com.daasuu.bl.BubbleLayout>
```
# Attributes
There are several attributes you can set:

| attr | description |
|:---|:---|
| bl_arrowWidth | Width of the arrow, default 8vp |
| bl_arrowHeight | Height of the arrow, default 8vp |
| bl_arrowPosition | Position of the arrow, default 12vp |
| bl_cornersRadius | Corner radius of the BubbleLayout, default vpp |
| bl_bubbleColor | Color of the BubbleLayout, default WHITE |
| bl_strokeWidth | Width of the stroke, default 0vp |
| bl_strokeColor | Color of the stroke, default GLAY |
| bl_arrowDirection | Drawing position of the arrow : 'left' or 'top' or 'right' or 'bottom' or 'left_center' or 'top_center' or 'right_center' or 'bottom_center' default 'left' |

# Samples

```xml
<com.daasuu.bl.BubbleLayout
        ohos:width="match_content"
        ohos:height="match_content"
        ohos:top_margin="12vp"
        ohos:padding="8vp"

        app:bl_arrowDirection="top"
        app:bl_arrowHeight="8vp"
        app:bl_arrowPosition="12vp"
        app:bl_arrowWidth="8vp"
        app:bl_bubbleColor="#ff33b5e5"
        app:bl_cornersRadius="8vp"
       >

        <DirectionalLayout
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:orientation="horizontal">

            <Image
                ohos:height="match_content"
                ohos:width="match_content"
                ohos:image_src="$media:ic_launcher"
                />

            <Text
                ohos:width="match_content"
                ohos:height="match_content"
                ohos:left_margin="4vp"
                ohos:layout_alignment="vertical_center"
                ohos:text="BubbleLayout"
                ohos:text_color="#ffcc0000"
                ohos:text_size="16fp"
                />
        </DirectionalLayout>

    </com.daasuu.bl.BubbleLayout>
```

```xml
<com.daasuu.bl.BubbleLayout
        ohos:width="match_content"
        ohos:height="match_content"
        ohos:top_margin="12vp"
        ohos:padding="8vp"

        app:bl_arrowDirection="right"
        app:bl_arrowHeight="8vp"
        app:bl_arrowPosition="16vp"
        app:bl_arrowWidth="8vp"
        app:bl_cornersRadius="6vp"
        app:bl_strokeWidth="1vp">

        <Text
            ohos:width="match_content"
            ohos:height="match_content"
            ohos:right_margin="4vp"
            ohos:layout_alignment="vertical_center"
            ohos:text="BubbleLayout"
            ohos:text_color="#ffcc0000"
            ohos:text_size="16fp"
            />
    </com.daasuu.bl.BubbleLayout>
```

```
# License

Copyright 2016 MasayukiSuda

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


